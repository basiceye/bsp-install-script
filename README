### DESCRIPTION ###

This repository contains a script for building a minimal custom RPi 2 BSP using the
ptxdist build system. On a successful finish, a bootable image file will be created that
can be directly flashed and used on the RPi 2.


### USAGE ###

NOTE:
The script requires root privileges to:
* add the pengutronix repository to your sources list
* upgrade your system and install the dependencies via package manager
* compile and install the build system (ptxdist) from source


### RUNNING THE SCRIPT ###

The script is completely automated! The only few things required from the user are:
clone/download it, allow it to run as a program, run it and provide root password.


### STAGES ###

The script has been broken down to six stages/functions, seven including the main body.
This was done primarily to separate different sections of script to ease the debugging
and code readability. The stages depend on each other and are executed in specific order
to ensure dependencies are met. Each stage is called from the main body and upon
successful execution it returns to the main body ready to continue to the next stage. If
execution is not successful, the stage exits from itself and the script with specific
error code and message.


### LOGS ###

As in any (almost) other does program, so does this script have it's logs. In particular,
this script has 3 group of logs: First group is comprised of one log
(last_successful_stage.txt) in which is actually stored only one number, number that
represents last successful stage. This log/number is used in case if script unexpectedly
exits, so when it's started again it can continue where it previously stopped. Second
group is also comprised of one log (script_states.txt) in which is in human readable
format explained what has happened in each state with everything in one place. Third
group consists of number (six) of logs, each one for each stage of the script.


### SCRIPT ARCHITECTURE ###

->Main
    |
    |	Initialization
    |	
    -> Stage One
        |
	| Installing necessary programs & dependencies ...
	| Updating and upgrading system ...
	|
    -> Stage Two
	|
	| Adding Pengutronix repository to sources list ...
	| Installing toolchain ...
	|
    -> Stage Three
	|
	| Installing ptxdist ...
	|
    -> Stage Four
	|
	| Downloading BSP ...
	|
    -> Stage Five
	|
 	| Configuring image ...
 	|
    -> Stage Six  
	|
        | Creating image ...
 	|


### KNOWN ISSUES ###

The script is known to fail if the sources for open source packages that will make up the
userspace are unavailable. That happens due to broken download links. In case that happens
you have to download the sources manually. 
Example:

If you get an error that the 'acl-2.2.49.src.tar.gz' package cannot be
downloaded/installed, you have to manually google it and download it yourself (in this
case you will have to download it for example from this page but not necessarily:
http://pkgs.fedoraproject.org/repo/pkgs/acl/acl-2.2.49.src.tar.gz/181445894cca986da9ae0099d5ce2d08/). 
After you download it, you have to move/copy it to
the 'src' folder inside 'bsp' folder.

Once finished all that, you can continue creating your image by running this command
inside 'bsp' folder:
 ptxdist images

Rinse and repeat for every package of this kind.


### HOW TO CLONE THE SCRIPT ###

First install 'git' if you don't have it:

sudo apt-get install git

Clone it from repository to your home folder:

cd
git clone https://bitbucket.org/basiceye/bsp-install-script.git


### ALLOW EXECUTING AS A PROGRAM ###

chmod +x custom_rootfs_script


### EXECUTE ###

./custom_rootfs_script


### RPi 2 DEPLOYMENT ###

When the script finishes successfully it will create an image
`~/bsp-install-script/platform-rpi2/images/hd.img`

That image contains the kernel, bootloader, rootfs and custom RPi2 boot config files.
Everything is placed in the correct partition layout so the image needs just to be
flashed on the SD card.
Below is an example given that your SD card is /dev/sdb (HINT: 'dmesg').

NOTE: Be sure to unmount the card before flashing.

Example
`sudo dd bs=4MB if=~/bsp-install-script/platform-rpi2/images/hd.img of=/dev/sdb`

Using 'dd' this way you won't see any progress on your screen, so be patient and wait for
the completion. The process should take around 5 minutes, depending on specifications of
your system and SD card.


### ALL YOU NEED TO DO ###
#!bash

sudo apt-get install git
git clone https://bitbucket.org/basiceye/bsp-install-script.git
cd bsp-install-script
chmod +x custom_rootfs_script
./custom_rootfs_script
